package com.quwi.app.ui.member.area

import android.media.Rating
import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.quwi.app.data.model.Project

class ProjectsDiffCallback(private val oldList: List<Project>, private val newList: List<Project>) :
    DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return oldList[oldPosition] == (newList[newPosition])
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}