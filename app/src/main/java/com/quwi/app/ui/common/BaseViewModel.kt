package com.quwi.app.ui.common

import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.ViewModel
import com.common.SingleLiveEvent
import com.jakewharton.rxbinding3.view.clicks
import com.jakewharton.rxbinding3.view.focusChanges
import com.jakewharton.rxbinding3.widget.textChangeEvents
import com.jakewharton.rxbinding3.widget.textChanges
import com.quwi.app.common.NetManager
import com.quwi.app.data.model.ServerError

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

import java.util.concurrent.TimeUnit


abstract class BaseViewModel(
    private val netManager: NetManager
) : ViewModel() {

    private var compositeDisposable = CompositeDisposable()
    private val _lvMessage = SingleLiveEvent<String>()
    private val _lvProgress = SingleLiveEvent<Boolean>()

    val lvProgress: SingleLiveEvent<Boolean>
        get() = _lvProgress
    val lvMessage: SingleLiveEvent<String>
        get() = _lvMessage


    fun clearDisposable() {
        compositeDisposable.clear()
        _lvProgress.postValue(false)
    }

    private fun terminateProgress(): Action? {
        return Action {
            _lvProgress.postValue(false)
        }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun addDisposable(d: Disposable?) {
        d?.let {
            compositeDisposable.add(it)
        }
    }


    fun isInternetAvailable(): Boolean {
        return netManager.isInternetAvailable.apply {
            if (!this) {
                lvMessage.value = netManager.message
                lvProgress.value = false
            }
        }
    }


    private fun consumeError(): Consumer<Throwable> {
        return Consumer {
            lvMessage.value = ServerError(it).message
        }
    }

    private fun subscribeProgress(): Consumer<Disposable> {
        return Consumer {
            _lvProgress.value = true
        }
    }


    open fun addClicks(
        view: View,
        callback: (() -> Unit)?
    ) {
        addDisposable(
            view.clicks()
                .debounce(300, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(Consumer {
                    callback?.invoke()
                }, consumeError())
        )
    }

    fun addTextWatcher(
        view: EditText,
        debounce: Long,
        callback: ((s: CharSequence) -> Unit)?
    ) {
        addDisposable(
            view.textChanges()
                .debounce(debounce, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(Consumer {
                    callback?.invoke(it)
                }, consumeError())

        )
    }


    fun addTextWatcher(
        view: TextView,

        callback: ((s: CharSequence) -> Unit)?
    ) {
        addDisposable(
            view.textChanges()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(Consumer {
                    callback?.invoke(it)
                }, consumeError())

        )
    }

    fun <T> compositeWrapper(
        source: Observable<T>,
        callback: ((s: T) -> Unit)?
    ): Disposable {
        return source
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnTerminate(terminateProgress())
            .doOnSubscribe(subscribeProgress())
            .subscribe(Consumer {
                callback?.invoke(it)
            }, consumeError())
    }

    fun <T> compositeWrapper(
        source: Observable<T>,
        delay: Long,
        callback: ((s: T) -> Unit)?
    ): Disposable {
        return source
            .subscribeOn(Schedulers.io())
            .delay(delay, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .doOnTerminate(terminateProgress())
            .doOnSubscribe(subscribeProgress())
            .subscribe(Consumer {
                callback?.invoke(it)
            }, consumeError())
    }

    fun <T> compositeWrapper(
        source: Observable<T>,
        result: ((s: T) -> Unit)?,
        throwable: ((s: Throwable) -> Unit)?
    ): Disposable {
        return source
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnTerminate(terminateProgress())
            .doOnSubscribe(subscribeProgress())
            .subscribe({
                result?.invoke(it)
            }, throwable)
    }

    fun <T> compositeWrapper(
        source: Observable<T>,
        errorHandler: Consumer<Throwable> = consumeError(),
        callback: ((s: T) -> Unit)?
    ): Disposable {
        return source
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnTerminate(terminateProgress())
            .doOnSubscribe(subscribeProgress())
            .subscribe(Consumer {
                callback?.invoke(it)
            }, errorHandler)
    }


    inline fun addTextValidate(
        view: TextView,
        crossinline setupErrorVisible: ((f: Boolean) -> Unit)
    ) {
        addDisposable(
            view.focusChanges()
                .skipInitialValue()
                .flatMap { hasFocus ->
                    return@flatMap view.textChanges()
                        .skipInitialValue()
                        .map {
                            setupErrorVisible(hasFocus)
                        }
                        .skipWhile { hasFocus }
                }
                .subscribe {

                }
        )
    }


}