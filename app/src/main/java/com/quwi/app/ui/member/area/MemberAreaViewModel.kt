package com.quwi.app.ui.member.area

import com.common.SingleLiveEvent
import com.quwi.app.common.NetManager
import com.quwi.app.data.model.Project
import com.quwi.app.data.sources.user.UserRepository
import com.quwi.app.data.sources.user.remote.requests.UpdateProjectParams
import com.quwi.app.ui.common.BaseViewModel
import javax.inject.Inject

class MemberAreaViewModel @Inject constructor(
    netManager: NetManager,
    val repository: UserRepository
) : BaseViewModel(netManager) {

    private val _lvProjects = SingleLiveEvent<List<Project>>()
    private val _lvUpdatedProject = SingleLiveEvent<Project>()
    val lvProjects: SingleLiveEvent<List<Project>>
        get() = _lvProjects

    val lvUpdatedProject: SingleLiveEvent<Project>
        get() = _lvUpdatedProject

    var selectedProject: String? = null

    fun getProjects() {
        if (isInternetAvailable()) {
            addDisposable(compositeWrapper(repository.getProjects()) {
                _lvProjects.value = it.projects
            })
        }
    }

    fun updateProjects(name: String) {
        if (isInternetAvailable())
            selectedProject?.let { id ->
                addDisposable(
                    compositeWrapper(
                        repository.updateProject(
                            id,
                            UpdateProjectParams(name)
                        )
                    ) {
                        _lvUpdatedProject.value = it
                    })
            }

    }

}