package com.quwi.app.ui.project

import androidx.annotation.Nullable
import androidx.recyclerview.widget.DiffUtil
import com.quwi.app.data.model.Project
import com.quwi.app.data.model.User

class UserDiffCallback (private val oldList: List<User>, private val newList: List<User>) :
    DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return oldList[oldPosition] == (newList[newPosition])
    }

    @Nullable
    override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
        return super.getChangePayload(oldPosition, newPosition)
    }
}