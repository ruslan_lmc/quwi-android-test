package com.quwi.app.ui.common

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.navigation.NavController
import androidx.navigation.Navigation
import dagger.android.support.DaggerAppCompatActivity


abstract class BaseActivity : DaggerAppCompatActivity() {

    protected abstract fun getLayoutResource(): Int

    protected abstract fun getNavigationResource(): Int

    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (getLayoutResource() != 0)
            super.setContentView(getLayoutResource())

        navController = Navigation.findNavController(this, getNavigationResource())
    }
}