package com.quwi.app.ui.member.area


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.quwi.app.R
import com.quwi.app.common.extensions.setRoundImage
import com.quwi.app.data.model.Project
import kotlinx.android.synthetic.main.item_project.view.*

class ProjectAdapter(
    private var onItemClick: ((Project) -> Unit)?,
    private var onLongItemClick: ((Project) -> Unit)?
) : RecyclerView.Adapter<ProjectAdapter.ViewHolder>() {

    private val items = ArrayList<Project>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_project, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun submitData(newRating: List<Project>) {
        val diffCallback = ProjectsDiffCallback(items, newRating)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        items.clear()
        items.addAll(newRating)
        diffResult.dispatchUpdatesTo(this)
    }

    fun updateItem(id: String, item: Project) {
        val position = items.indexOfFirst {
            it.id == id
        }
        items[position] = item
        notifyItemChanged(position)

    }


    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(items[adapterPosition])
            }
            itemView.setOnLongClickListener {
                onLongItemClick?.invoke(items[adapterPosition])
                true
            }
        }

        fun bind(item: Project) {
            itemView.item_project_tv_name.text = item.name
            itemView.item_project_iv_icon.setRoundImage(url = item.logo_url)
        }
    }
}