package com.quwi.app.data.sources.user.remote

import com.quwi.app.data.model.Project
import com.quwi.app.data.sources.user.remote.requests.UpdateProjectParams
import com.quwi.app.data.sources.user.remote.requests.LoginParams
import com.quwi.app.data.sources.user.remote.response.LoginResponse
import com.quwi.app.data.sources.user.remote.response.ProjectInfoResponse
import com.quwi.app.data.sources.user.remote.response.ProjectsResponse
import com.quwi.app.data.sources.user.remote.response.UpdateProjectResponse
import io.reactivex.Observable
import retrofit2.http.*

interface UserApi {
    @POST("auth/login")
    fun login(@Body params: LoginParams): Observable<LoginResponse>
    @GET("projects-manage/index")
    fun getProjects(): Observable<ProjectsResponse>
    @POST("projects-manage/update")
    fun updateProject(@Query("id") id : String, @Body body : UpdateProjectParams): Observable<UpdateProjectResponse>
    @GET("projects-manage/{id}")
    fun getProjectsInfo(@Path("id") id : String): Observable<ProjectInfoResponse>
}