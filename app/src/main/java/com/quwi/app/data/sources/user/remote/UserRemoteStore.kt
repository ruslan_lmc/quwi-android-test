package com.quwi.app.data.sources.user.remote

import com.quwi.app.data.model.Project
import com.quwi.app.data.sources.user.remote.requests.UpdateProjectParams
import com.quwi.app.data.sources.user.remote.requests.LoginParams
import com.quwi.app.data.sources.user.remote.response.LoginResponse
import com.quwi.app.data.sources.user.remote.response.ProjectInfoResponse
import com.quwi.app.data.sources.user.remote.response.ProjectsResponse
import com.quwi.app.data.sources.user.remote.response.UpdateProjectResponse
import io.reactivex.Observable
import javax.inject.Inject

class UserRemoteStore @Inject constructor(
    private val api: UserApi
) {
    fun login(params: LoginParams): Observable<LoginResponse> = api.login(params)
    fun getProjects(): Observable<ProjectsResponse> = api.getProjects()
    fun updateProject(id: String, body: UpdateProjectParams): Observable<UpdateProjectResponse> =
        api.updateProject(id, body)
    fun getProjectsInfo(id: String): Observable<ProjectInfoResponse> =
        api.getProjectsInfo(id)

}