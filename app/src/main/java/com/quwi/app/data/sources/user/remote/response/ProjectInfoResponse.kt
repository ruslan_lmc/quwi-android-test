package com.quwi.app.data.sources.user.remote.response

import com.quwi.app.data.model.ProjectInfo

data class ProjectInfoResponse(
    val project: ProjectInfo
)