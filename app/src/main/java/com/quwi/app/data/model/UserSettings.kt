package com.quwi.app.data.model

data class UserSettings(
    val client_settings: ClientSettings,
    val dta_mute_until: Any,
    val is_mute_chats: Boolean,
    val lang: String,
    val mute_until_period: Int
)