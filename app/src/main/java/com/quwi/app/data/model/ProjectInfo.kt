package com.quwi.app.data.model

data class ProjectInfo(
    val id: Int,
    val is_active: Int,
    val is_owner_watcher: Int,
    val logo_url: String,
    val name: String,
    val position: Int,
    val spent_time_all: Int,
    val spent_time_month: Int,
    val spent_time_week: Int,
    val uid: String,
    val users: List<User>?
)