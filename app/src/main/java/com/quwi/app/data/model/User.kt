package com.quwi.app.data.model

import java.util.*

data class User(
    val avatar_url: String?,
    val dta_activity: String?,
    val dta_since: String?,
    val id: Int,
    val is_online: Int,
    val name: String



)