package com.quwi.app.data.model

data class Profile(
    val allow_timer_autostop: Boolean,
    val allow_timer_screens: Boolean,
    val avatar_url: Any,
    val dta_activity: String,
    val dta_create: String,
    val dta_timer_activity: Any,
    val due_penalties: Int,
    val email: String,
    val email_signature: Any,
    val id: Int,
    val id_company: Int,
    val is_active: Boolean,
    val is_online: Int,
    val is_shared_for_me: Boolean,
    val is_shared_from_me: Boolean,
    val is_timer_online: Int,
    val name: String,
    val projects: List<Any>,
    val role: String,
    val timer_last: Any,
    val timezone_offset: Int
)