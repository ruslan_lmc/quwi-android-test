package com.quwi.app.data.sources.user

import com.quwi.app.data.model.Project
import com.quwi.app.data.sources.user.local.SessionPreference
import com.quwi.app.data.sources.user.remote.UserRemoteStore
import com.quwi.app.data.sources.user.remote.requests.UpdateProjectParams
import com.quwi.app.data.sources.user.remote.requests.LoginParams
import com.quwi.app.data.sources.user.remote.response.LoginResponse
import com.quwi.app.data.sources.user.remote.response.ProjectInfoResponse
import com.quwi.app.data.sources.user.remote.response.ProjectsResponse
import io.reactivex.Observable
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val remoteStore: UserRemoteStore,
    private val sessionStore: SessionPreference
) {
    fun login(params: LoginParams): Observable<LoginResponse> = remoteStore.login(params).flatMap {
        sessionStore.token = it.token
        return@flatMap Observable.just(it)
    }

    fun getProjects(): Observable<ProjectsResponse> = remoteStore.getProjects()
    fun updateProject(id: String, body: UpdateProjectParams): Observable<Project> =
        remoteStore.updateProject(id, body).flatMap {
            return@flatMap Observable.just(it.project)
        }

    fun isAuthorized(): Boolean = !sessionStore.token.isNullOrEmpty()
    fun getProjectsInfo(id: String): Observable<ProjectInfoResponse> =
        remoteStore.getProjectsInfo(id)
}