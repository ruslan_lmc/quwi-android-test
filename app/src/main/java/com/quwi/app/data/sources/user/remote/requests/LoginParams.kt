package com.quwi.app.data.sources.user.remote.requests

data class LoginParams(
    val email: String,
    val password: String
)