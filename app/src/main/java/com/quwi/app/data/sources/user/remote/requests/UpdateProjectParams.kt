package com.quwi.app.data.sources.user.remote.requests

data class UpdateProjectParams (val name : String)