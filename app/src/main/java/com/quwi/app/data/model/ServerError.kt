package com.quwi.app.data.model

import android.util.Log

import com.google.gson.JsonParser
import retrofit2.HttpException


class ServerError constructor(error: Throwable) {

    var statusCode: Int = 0
    var message: String = "An error occurred"

    init {
        val sb = StringBuffer("")
        if (error is HttpException) {

            val errorJsonString = error.response()
                .errorBody()?.string()
            try {
                JsonParser().parse(errorJsonString).apply {
                    try {
                        asJsonObject["first_errors"]?.let { json ->
                            if (json.isJsonObject) {
                                val email = json.asJsonObject["email"]?.asString
                                email?.let {
                                    sb.append(it)
                                }
                            }
                        }

                    } catch (e: Exception) {
                        e.message?.let {
                            Log.e(ServerError::class.simpleName, it)
                        }
                    }
                }

            } catch (e: Exception) {
                e.message?.let {
                    Log.e(ServerError::class.simpleName, it)
                }
            }
            this.statusCode = error.code()
        }
        if (sb.isEmpty()) {
            val errorMessage = error.message
            if (!errorMessage.isNullOrEmpty())
                sb.append(errorMessage)
            else
                sb.append("An error occurred")
        }
        this.message = sb.toString()
    }
}

