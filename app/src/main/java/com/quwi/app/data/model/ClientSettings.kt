package com.quwi.app.data.model

data class ClientSettings(
    val hash: String,
    val issues_filters: List<IssuesFilter>
)