package com.common

import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.widget.AppCompatEditText
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxStringCompare(private var pattern: String) : TextWatcher {

    var subject: PublishSubject<Boolean> = PublishSubject.create()
    private lateinit var editText: AppCompatEditText


    fun fromEditText(editText: AppCompatEditText): Observable<Boolean> {
        this.editText = editText
        this.editText.addTextChangedListener(this)
        return subject
    }

    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(text: CharSequence?, start: Int, before: Int, count: Int) {
        this.editText.removeTextChangedListener(this)
        text?.let { chars ->
            subject.onNext(isMatch(chars.toString()))
            this.editText.addTextChangedListener(this)
        }
    }

    private fun isMatch(value: String): Boolean = value.isNotEmpty() && value == pattern

    fun isMatch(): Boolean = isMatch(editText.text.toString())

    fun complete() {
        subject.onComplete()
    }
}