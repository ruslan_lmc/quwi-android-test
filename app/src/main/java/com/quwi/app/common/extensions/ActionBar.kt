package com.quwi.app.common.extensions

import androidx.appcompat.app.ActionBar

fun ActionBar.setupHomeButtonAndTitle(
    showHomeButton: Boolean = true,
    homeAsUpEnabled: Boolean = true,
    title: String? = null
) {
    setDisplayShowHomeEnabled(showHomeButton)
    setDisplayHomeAsUpEnabled(homeAsUpEnabled)
    setDisplayShowTitleEnabled(true)
    setTitle(title)
}