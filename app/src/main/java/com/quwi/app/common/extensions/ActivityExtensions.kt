package com.quwi.app.common.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar



inline fun <reified T : ViewModel> FragmentActivity.injectViewModel(factory: ViewModelProvider.Factory): T {
    return ViewModelProvider(this, factory)[T::class.java]
}

inline fun <reified T> Activity.navigateTop() {
    val intent = Intent(this, T::class.java)
    startActivity(intent)
}


inline fun <reified T> Activity.navigateTop(bundle: Bundle?) {
    val intent = Intent(this, T::class.java)
    bundle?.let {
        intent.putExtras(it)
    }
    startActivity(intent)
}


fun Activity.navigateClearTop(destination: Class<*>, arg: String) {
    val intent = Intent(this, destination)
    intent.addArg(arg)
    intent.clearTop()
}

fun Activity.createIntent(clazz : Class<*>): Intent = Intent(this, clazz)
inline fun <reified T> Activity.createIntent(): Intent = Intent(this, T::class.java)

fun Intent.addArg(arg: String): Intent {
    this.putExtra("ARG", arg)
    return this
}


fun Intent.getArg(): String? {
    return this.getStringExtra("ARG")
}

fun Intent.clearTop(): Intent {
    this.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
    return this
}

inline fun <reified T> Activity.navigateClearTop() {
    val intent = Intent(this, T::class.java)
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
    startActivity(intent)
}


inline fun <reified T> Activity.navigateClearTop(params: Map<String, String>) {
    val intent = Intent(this, T::class.java)
    for ((k, v) in params) {
        intent.putExtra(k, v)
    }
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
    startActivity(intent)
}

inline fun <reified T> Activity.navigateClearTop(bundle: Bundle?) {
    val intent = Intent(this, T::class.java)
    bundle?.let {
        intent.putExtras(it)
    }
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
    startActivity(intent)
}

fun Activity.showSoftKeyboard(view: View?) {
    if (view != null) {
        val imm =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }
}



fun Activity.showForceSoftKeyboard() {
    val imm =
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(
        InputMethodManager.SHOW_FORCED,
        InputMethodManager.HIDE_IMPLICIT_ONLY
    )
}


fun Activity.showSnack(message: String?) {
    if (message != null) {
        val view = findViewById<View>(android.R.id.content)
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show()
    }
}

fun Activity.hideSoftKeyboard(view: View?) {
    if (view != null) {
        val imm = view
            .context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}


fun FragmentActivity.showDialog(
    title: String,
    message: String,
    positive: String,
    callback: (() -> Unit)?
) {
    val builderSingle = MaterialAlertDialogBuilder(this).apply {
        setTitle(title)
        setMessage(message)
        setPositiveButton(positive) { dialog, _ ->
            dialog.dismiss()
            callback?.invoke()
        }
        setCancelable(false)
    }
    if (!isFinishing)
        builderSingle.show()
}


fun FragmentActivity.showDialog(
    title: Int,
    message: Int,
    callback: (() -> Unit)?
) {
    val builderSingle = MaterialAlertDialogBuilder(this).apply {
        setTitle(title)
        setMessage(message)
        setPositiveButton(android.R.string.ok) { dialog, _ ->
            dialog.dismiss()
            callback?.invoke()
        }
        setCancelable(false)
    }

    builderSingle.show()
}







