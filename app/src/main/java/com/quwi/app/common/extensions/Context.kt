package com.quwi.app.common.extensions

import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat

fun Context.getCompatColor(res: Int): Int {
    return ContextCompat.getColor(this, res)
}
inline fun<reified T> Context.createIntent(): Intent = Intent(this, T::class.java)