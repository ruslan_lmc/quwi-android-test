package com.quwi.app.di

import android.app.Application
import com.quwi.app.QuwiApplication
import com.quwi.app.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            ActivityModule::class,
            ViewModelModule::class,
            NetworkClientModule::class,
            NetworkApiModule::class,
            RepositoryModule::class,
            FragmentBuildersModule::class,
            HelpersModule::class
        ]
)
interface AppComponent : AndroidInjector<QuwiApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent

    }

}