package com.quwi.app.di.modules


import android.app.Application
import android.content.Context
import com.quwi.app.common.NetManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class HelpersModule {

    @Singleton
    @Provides
    fun provideAppPreference(application: Application): NetManager {
        return NetManager(application)
    }


}