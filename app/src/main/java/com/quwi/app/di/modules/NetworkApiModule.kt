package com.quwi.app.di.modules

import com.quwi.app.data.sources.user.remote.UserApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class NetworkApiModule {
    @Provides
    internal fun provideUserApi(retrofit: Retrofit): UserApi {
        return retrofit.create(UserApi::class.java)
    }
}