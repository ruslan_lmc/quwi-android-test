package com.quwi.app.di.modules

import com.quwi.app.di.annotation.ActivityScoped
import com.quwi.app.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityModule{

    @ActivityScoped
    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun mainActivity(): MainActivity


}