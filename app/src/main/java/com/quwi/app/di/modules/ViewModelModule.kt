package com.quwi.app.di.modules


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.common.AppViewModelFactory
import com.quwi.app.di.annotation.ViewModelKey
import com.quwi.app.ui.login.LoginViewModel
import com.quwi.app.ui.member.area.MemberAreaViewModel
import com.quwi.app.ui.project.ProjectPageViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Suppress("unused")
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MemberAreaViewModel::class)
    abstract fun bindMemberAreaViewModel(viewModel: MemberAreaViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProjectPageViewModel::class)
    abstract fun bindProjectPageViewModel(viewModel: ProjectPageViewModel): ViewModel



    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelFactory): ViewModelProvider.Factory
}