package com.quwi.app.di.modules

import android.app.Application
import android.content.Context
import com.quwi.app.data.sources.user.local.SessionPreference
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun provideSessionPreference(application: Application): SessionPreference {
        return SessionPreference(application.getSharedPreferences("app_setting", Context.MODE_PRIVATE))
    }


}